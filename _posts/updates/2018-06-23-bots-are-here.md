---
layout: post
title:  "[Reles] Bots are here with 0.0.4!"
date:   2018-06-23 16:00:00 -0700
categories: blog
tags:
  - autobotnet
  - update
authors:
  - ff
---

Bots and cores are finally functional!

Install a mining core and a storage core into your bot, and `drill` for ore.

Here is the changelog:

### `0.0.4-dev` "Clink" (June 23, 2018)

- major overhaul of client script api
    - new client api modules
        - `MapModule`, `ArmyModule`, `GameApiModule`, `UtilsModule`, `ConstantsModule` replace old globals
    - automatic spawn placement and room generation
    - bot "core" system based on design
        - modular data-driven bot cores
        - cores define stats of bots
        - cores provide custom functions (ex. "drill")
    - realtime and queued push notifications
    - bot reference sandboxing through `ProxyRef` objects: (`GameEntityRef`) that safely forward read-only data fields
- extensibility expansions
    - nearly every "content" part of the game can be added to without need for reflection or hooking
- global real time evented system
    - for connecting to client frames
    - events are automatically restricted to bots sensor range
- improved metrics data control and allow full data export

More ETA Son.
