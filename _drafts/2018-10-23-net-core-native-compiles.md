---
layout: post
title:  ".NET Core Native Compiles"
date:   2018-10-22 18:00:00 -0700
categories: guides
tags:
  - dotnet
authors:
  - ff
---

.NET Core has a native compiler using CoreRT in the works! It is a cross-platform evolution of Microsoft's [.NET Native](https://docs.microsoft.com/en-us/dotnet/framework/net-native/)
technology used for deploying .NET Framework and Windows 10 apps by compiling to native code.

## Why native compile?

## Why not native compile?

