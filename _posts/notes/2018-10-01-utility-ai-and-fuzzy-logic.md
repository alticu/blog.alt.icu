---
layout: post
title:  "[Notes] Utility AI and Fuzzy Logic"
date:   2018-10-01 15:00:00 -0700
categories: notes
tags:
  - simdev
authors:
  - ff
---

Utility AI
==========

Overview (Utility Theory)
-------------------------

-   Utility theory is based on selecting the most \"useful\" action
    given a certain state.
-   Utility AI represents a utility function and selects associated
    actions based on the consideration with the \"most optimal\" utility
    value

Components
----------

### Reasoner

-   the root of a utility AI
-   ranks the considerations based on certain rules, scored
    considerations become \"options\"

### Appraisal

-   calculates and returns a score
-   used by the consideration

### Consideration

-   contains a list of appraisals and an action
-   calculates a combination score based on the appraisal scores
    -   this score represents the \"utility\" of its associated action

### Action

-   the result of a consideration being selected

Implementation
--------------

### Reasoner

list of considerations

-   addConsideration
-   execute

### Appraisal

-   score(context)

### Consideration

-   types of considerations
    -   sum - adds the appraisal scores, returns sum
    -   threshold - returns 0 if any appraisal does not score above
        threshold, returns sum
    -   thresholdsum - adds the appraisal scores, returns 0 if sum is
        below threshold, otherwise returns sum

Fuzzy logic with Utility AI
===========================

Why fuzzy logic?
----------------

-   utility ai is by nature already less predictable than other
    traditional forms of game AI, including state machines and behavior
    trees
-   fuzzy logic encourages \"exploration\" of possible opportunities in
    a choice that may not always be the most optimal
-   using fuzzy logic is a tradeoff: it brings increased variation and
    deviation from strict rules, but it also involves choosing options
    that scored lower than the optimal, possibly evene significantly
    lower

Methods of introducing \"fuzziness\"
------------------------------------

### Simple random fuzziness

-   takes the top \$N\$% of ranked options as \"candidates\"
-   randomly selects a candidate

### Possible random fuzziness

-   with an \$M\$% chance uses the \"simple fuziness\" method, otherwise
    select the best option

### Score-aware fuzziness

-   the previous two fuzziness methods do not take into account scores
    at all; this could mean that when one option ranks far higher than
    others, one of the others that have much lower scores may be chosen
-   this fuzziness method adjusts for the difference in scores between
    possible option
-   by using a special probability distribution function, as the
    relative \"x-value\" of other scores increases, there is a lower
    probability that suboptimal alternatives will be chosen
    -   options are chosen by using the inverse $N$ of that probability
        distribution function at a certain \"x-value\", and taking the
        option with the lowest score that is above $N$
-   this can be combined with \"simple\" and \"possible\" fuzziness to
    get a fuzziness method that
    -   only considers high-ranking options
    -   does not always choose randomly
    -   is aware of the score distribution of its options
