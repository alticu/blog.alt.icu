---
layout: post
title:  "[Reles] Engine improvements and features with 0.0.5!"
date:   2018-08-18 20:00:00 -0700
categories: blog
tags:
  - autobotnet
  - update
authors:
  - ff
---

This update had a lot of internal engine changes and improvements.
A basic level of world interaction is now available! Resources can be drilled from terrain!
Plugins have been improved to automatically cache for higher performance and to support plugins in even more areas, such as registering custom ores and resources.

Changelog:

## `0.0.5-dev` "Memeory" (August 18, 2018)

- performance tweaks and optimizations
    - optimized room pack format to be gzipped binary; this new format is also used in database serialization
    - types from plugins are cached for quick indexed lookup in `ItemRegistry`
- map interaction support
    - drilling support
        - drill using the `CoreDrill`, by calling the `drill` action
        - tiles now have an associated `durability` that represents the amount of drill power (drill core tier) required
        - drilling transfers resources from tiles to an empire
    - `TileDeltaEvent` for when tiles change
- renamed `UserTeam` to `UserEmpire` to be more semantic (it represents a single empire, not a "team" or "alliance")
- entities feature development
    - multiple entities can no longer inhabit the same space
        - enforced when creating new bots from factory and in `MobileEntity::move`
    - base for buildings (`BuildingEntity`), which have custom actions that can be called with `.call`
    - entities now have a health value (max health comes from template)
    - Bots can now have a move cost, which is a cooldown before they can move
    - Bots have a small data storage array, where they can store persistent data. values can be set using `.mset`
- gameplay
    - you can now surrender to destroy your team, starting a cooldown before you can `boot` again as a new empire

More ETA Son.
