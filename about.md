---
layout: page
title: About
permalink: /about/
---

<!-- <div class="center pad-around" style="margin-bottom: 40px;">
  <img src="{{ site.url }}/content/logo.png" width="128" />
  <h3 style="margin-top: 10px;">AutoBotnet</h3>
</div> -->

<img class="m-image" src="/assets/img/banner.png" alt="alticu banner" />
<div class="m-button m-primary">
  <a href="https://alt.icu">ALTiCU Home</a>
</div>
