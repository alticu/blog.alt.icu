---
layout: post
title:  "Welcome to AutoBotnet!"
date:   2017-08-09 21:42:03 -0700
categories: meta
tags:
  - meta
  - autobotnet
authors:
  - ff
  - joona
---

<div class="center pad-around" style="margin-bottom: 40px;">
  <img src="{{ site.url }}/content/logo.png" width="128" />
  <h3 style="margin-top: 10px;">AutoBotnet</h3>
</div>

AutoBotnet is a scripting based RTS, Coming Soon™
Brought to you by The CookieEaters.
