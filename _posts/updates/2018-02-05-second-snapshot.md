---
layout: post
title:  "[Reles] Second snapshot released!"
date:   2018-02-05 20:55:00 -0700
categories: blog
tags:
  - autobotnet
  - update
authors:
  - ff
---

This week, we resumed development at full speed after an extended period of inactivity.

After a significant amount of fixes and improvements, we're releasing [development snapshot
`0.0.2-dev`](https://github.com/CookieEaters/AutoBotnet/releases/tag/0.0.2-dev) code named "Illeg."

Here is the changelog:

### `0.0.2-dev` "Illeg" (February 5, 2018)

- Upgrade to ASP.NET Core 2.0
- Significant improvements and fixes in the script execution engine, and support for security measures such as timeouts and recursion limits
- REST API improvements
- Security fix: password change triggers re-generation of API key
- Internal engine improvements
- Completely removed the WebUI in favor of future native desktop-based and terminal-based clients (Will be Windows, macOS, and GNU/Linux)
- Updated documentation, includes metadata endpoints, code deployment, and complete and detailed documentation of authentication endpoints

More ETA Son.
