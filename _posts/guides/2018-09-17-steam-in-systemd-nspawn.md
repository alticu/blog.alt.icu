---
layout: post
title:  "Running steam in systemd-nspawn"
date:   2018-09-17 19:33:03 -0700
categories: guides
tags:
  - linux
authors:
  - joona
---

Running Steam on your Linux PC can be annoying, mainly because of all the 32-bit dependencies.  

## Host requirements

The following things need to be set up on the host system:
* User namespaces
* cgroups
* systemd
* X11
* pulseaudio
* paprefs

## Container creation

I used Ubuntu 18.04 bionic for my container, but you should be able to make any distribution work.

To create the container into a folder called `games`, run this command as root:  
```
# debootstrap --arch=amd64 bionic games
```

*Note: I'm creating a 64bit container instead of 32bit because Steam had issues running in a 32bit container*

## Initial setup

To log in for the first time, run this as root:  
```
# systemd-nspawn -UD games
```

Go ahead and set a root password, and create a user for yourself.  
You can then log in using:
```
$ machinectl login games
```

### Display

I chose to start a second X server for the container to avoid games messing with my display.  
Unfortunately, hardware acceleration does not work in XNest or Xephyr :(

Start the X server you wish to use, and use `$ xhost +local:` to allow the container to connect to it

After that, you can start the container with this (replace X1 with X0 if you're using your primary X server):
```
# systemd-nspawn --bind=/tmp/.X11-unix/X1 --bind=/dev/dri -bUD
```

That command binds the second X server and the graphics card, then runs the container in the background.  

Log into the container, and add `DISPLAY=0` to `/etc/environment`

### Audio

I was unable to get PulseAudio working through a unix socket, so you need to enable remote connections using `paprefs`.  
After that, you can add `PULSE_SERVER=127.0.0.1` to `/etc/environment` in the container.

## Using

After logging into the container and reloading `/etc/environment`, you should be able to run X11 applications!  
You can simply install Steam as normal, and *most* games will just work.

## Issues

~~Vulkan doesn't seem to work in the container, see [this thread](https://bbs.archlinux.org/viewtopic.php?pid=1807741) for details.~~  
Update: This has now been resolved, adding `--property=DeviceAllow="/dev/dri/renderD128"` seems to fix it.

If you have any issues, feel free to [contact me](https://alt.icu/about/contact/)!
