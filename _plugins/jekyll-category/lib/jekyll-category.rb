def check_add(hash, key, val)
    if not hash.has_key? key then
        hash[key] = Array.new
    end
    hash[key].push(val)
end

module Jekyll
    class CategoryPage < Page
        def initialize(site, base, dir, title, posts)
            @site = site
            @base = base
            @dir = dir
            @name = "index.html"

            self.process(@name)
            self.read_yaml(File.join(base, "_layouts"), "category.html")

            self.data['category'] = title
            self.data['posts'] = posts
        end
    end

    class CategoryPageGenerator < Generator
        safe true

        def generate(site)
            a_posts = Hash.new
            t_posts = Hash.new

            site.posts.docs.each do |post|
                post.data["authors"].each do |author|
                    check_add(a_posts, author, post)
                end
                
                post.data["tags"].each do |tag|
                    check_add(t_posts, tag, post)
                end
            end

            a_posts.each do |author, posts|
                a_nfo = site.config["authors"][author]
                site.pages << CategoryPage.new(site, site.source, a_nfo["username"], "by " + a_nfo["name"], posts)
            end

            t_posts.each do |tag, posts|
                site.pages << CategoryPage.new(site, site.source, "tag/" + tag, "tagged " + tag, posts)
            end
        end
    end
end
