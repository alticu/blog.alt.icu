---
layout: post
title:  "First development source snapshot released"
date:   2017-08-09 22:08:03 -0700
categories: blog
tags:
  - autobotnet
  - update
authors:
  - ff
---

Yesterday, we published our first complete source snapshot
to a [public repository on GitHub](https://github.com/CookieEaters/AutoBotnet).

The entirety of the AutoBotnet source code is licensed under the AGPLv3, and
all art assets are licensed under the CC-BY-SA-4.0.

The source snapshot comes with instructions to build the game from
source and configuration and scripts to allow quick development and modification.
Binary releases will become available as the game reaches the beta stage.

While the game is certainly not complete and is not really in a playable
state either, lots of progress has been made in developing the various parts of
the game server and infrastructure. In addition, our design planning
is taking place in a [public repository](https://github.com/CookieEaters/AutoBotnetDesign)
open to contribution from anyone.

We will periodically be pushing the `master` branch of our development
to this mirror repository.
